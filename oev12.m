A1 = 28;
A2 = 28;
a1 = 0.071;
a2 = 0.071;
k1 = 3.14;
k2 = 3.29;
km = 0.5;
g = 981;


% solve for h2 at h1 = 12.6cm
h1 = 12.6;
h2 = (a1/a2)^2*h1
u = a2*sqrt(2*g*h2)/k2

% with 12.6 as the bias point the sqrts become 1/(2*sqrt(2*g*h1))(h-h1)
% which with 12.6 is 0.0032(h-12.6)

