figure(1)
%Control object
G1 = tf( [0 1], [0.15 1] );
G2 = tf( [0 1], [1.5] );
G3 = tf( [1], [1 0 0] );

%Feedback transform
H = tf( [1], [1] );

%wanted phase margin
wPm = 60;

%adjust Kp
Kp = 6.5;
oloop1 = Kp*G1*G2*G3*H; 
margin(oloop1)

figure(1)
hold on
%choose wm = 5rad/s
wm = 5;
%at 5rad/s phase margin is -36deg, 55-(-36) = 91
alpha = 0.1
taud = 1/(wm*sqrt(alpha) )
lead = tf([taud 1], [alpha*taud 1] );
Kp = 1/abs(evalfr(lead,i*wm)*evalfr(oloop1,i*wm)/Kp)
Plead = Kp*lead;
oloop = Plead*G1*G2*G3*H;
margin( oloop )

cloop = feedback(Plead*G1*G2*G3,H);
margin(cloop)
hold off
figure(2)
step(cloop)
S = step(cloop)
bandwidth(cloop)

%check d^2x
cloop = feedback( Plead*G1*G2, G3*H )
hold on
%step(cloop)
S = stepinfo(cloop)
hold off

%move lead part to feedback path
cloop = feedback( Kp*G1*G2*G3, H*lead );
figure( 1 )
hold on
margin( cloop )
hold off
figure( 2 )
hold on
step( cloop )
S = stepinfo( cloop )
cloop = feedback( Kp*G1*G2, G3*H*lead );
step( cloop )
S = stepinfo( cloop )
hold off

%tune Kp and taud
taud = taud/0.45;
Kp = 0.12*Kp;
lead = tf([taud 1], [alpha*taud 1] );
cloop = feedback( Kp*G1*G2*G3, H*lead );
hold on
step( cloop, 'b--' )
S = stepinfo( cloop )
hold off
cloop = feedback( Kp*G1*G2, G3*H*lead );
hold on
step( cloop, 'y--' )
S = stepinfo( cloop )
hold off