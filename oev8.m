
figure(1)
%Control object
G1 = tf( [1], [0.00029 1] );
G2 = tf( [3.9], [0.00203 1] );

%Feedback transform
H = tf( [0.1], [0.00084 1] );

%wanted phase margin
wPm = 60;

%tune Kp to 60 degree phase margin
Kp = 7.9;
oloop1 = Kp*G1*G2*H; 
oloop2 = G1*G2*H;
cloop1 = feedback(Kp*G1*G2,H);
margin(oloop1)
figure(2)
step(cloop1)
figure(1)
[Gm, Pm, Wcg, Wcp]=margin(oloop1);


%choose taui reservation
iangle = -11

%choose lead alpha
alpha = 0.1;
newcross = -180+wPm-iangle-55 

%read freq: 1920rad/s
wm = 1920;
taud = 1/(wm*sqrt(alpha));
taui = 5/wm;
taud8 = taud
taui8 = taui
%calculate new Kp
Ilead = tf([taui 1], [taui 0])*tf([taud 1],[taud*alpha 1]);
Kp = 1/abs(evalfr(Ilead,i*wm)*evalfr(oloop1,i*wm)/Kp)
PID = Kp*Ilead;
oloop = PID*G1*G2*H;
cloop = feedback(PID*G1*G2,H);
hold on
margin(oloop)
margin(cloop)
hold off
figure(2)
step(cloop)
S = stepinfo( cloop )
Bwidth8 = bandwidth( cloop )
hold off

lead8= tf([taud 1],[taud*alpha 1]);
I8 = tf([taui 1], [taui 0]);

%check u(s)
cloop = feedback( Kp*I8*lead8, G1*G2*H);
figure(2)
hold on
step( cloop )
S = stepinfo( cloop )
hold off

%move lead part to feedback path
cloop = feedback(Kp*I8*G1*G2,H*lead8);
figure( 1 )
hold on
margin( cloop )
hold off 


figure(2)
hold on
S = stepinfo( cloop )
Bwidth9 = bandwidth( cloop )
step( cloop )
hold off

%tune parameters
hold on
taud = taud/2
Kp = 1.5*Kp
lead8= tf([taud 1],[taud*alpha 1]);
I8 = tf([taui 1], [taui 0]);
cloop = feedback(Kp*I8*G1*G2,H*lead8);
S = stepinfo( cloop )
bandwidth(cloop)
step( cloop )
hold off
figure( 1 )
hold on
margin(cloop)
hold off

%check u(s)
cloop = feedback( Kp*I8, G1*G2*H*lead8 );
figure(2)
hold on
step(cloop)
S = stepinfo( cloop )
hold off
%%
% exercise 10
% reuse closed loop system from exercise 8
orgcloop = feedback(PID*G1*G2,H);
figure( 3 )
step( orgcloop )
hold off
% add pre-filter with tauf = taud8
tauf = taud8
pref = tf( [1], [tauf 1] )
cloop = pref*orgcloop
hold on
step( cloop )
hold off
%adjust tauf
tauf = 0.5*tauf
pref = tf( [1], [tauf 1] )
cloop = pref*orgcloop
hold on
step( cloop )
hold off
figure(1)
hold on
margin(cloop)
hold off





