M = 0.5;
m = 0.2;
b = 0.1;
I = 0.006;
g = 9.8;
l = 0.3;

q = (M+m)*(I+m*l^2)-(m*l)^2;
b1 = m*l/q;
a2 = b*(I+m*l^2);
a1 = -(M+m)*m*g*l/q;
a0 = -b*m*g*l;


G = tf([b1 0], [1 a2 a1 a0] );

bode( G )